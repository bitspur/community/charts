apiVersion: integration.rock8s.com/v1beta1
kind: Socket
metadata:
  name: {{ template "s3.name" . }}
  labels:
    app.kubernetes.io/name: {{ template "s3.name" . }}
    helm.sh/chart: {{ .Chart.Name }}-{{ .Chart.Version }}
    app.kubernetes.io/instance: {{ .Release.Name }}
    app.kubernetes.io/managed-by: {{ .Release.Service }}
spec:
  epoch: {{ now | unixEpoch | quote }}
  interface:
    config:
      plug:
        bucket:
          required: true
        create: {}
        preserve: {}
        serviceAccountName: {}
      socket:
        endpoint:
          required: true
        accessKey: {}
        port: {}
        region: {}
        secretKey: {}
        tls: {}
{{- if (not (and .Values.endpoint .Values.accessKey .Values.secretKey)) }}
    result:
      socket:
        roleArn:
          required: true
{{- end }}
  config:
    tls: '1'
{{- if (and .Values.endpoint .Values.accessKey .Values.secretKey) }}
    endpoint: {{ .Values.endpoint | quote }}
{{- else }}
    region: '{{ template "s3.aws-region" . }}'
  configTemplate:
    endpoint: '{% printf "%s.s3.%s.amazonaws.com" .plug.spec.config.bucket "{{ template "s3.aws-region" . }}" %}'
{{- end }}
{{- if (and .Values.endpoint .Values.accessKey .Values.secretKey) }}
  configSecretName: '{{ template "s3.name" . }}-secret'
{{- else }}
  resultTemplate:
    roleArn: '{% .vars.roleArn %}'
  resultVars:
    - name: roleArn
      fieldref:
        fieldPath: status.roleArn
      objref:
        apiVersion: awsblueprints.io/v1alpha1
        kind: DelegateS3IRSA
        templateName: '{% .plug.metadata.name %}-{% .plug.metadata.namespace %}'
        namespace: {{ .Release.Namespace }}
{{- end }}
  resources:
{{- if (and .Values.endpoint .Values.accessKey .Values.secretKey) }}
    - when: [coupled, updated]
      do: recreate
      stringTemplate: |
        apiVersion: batch/v1
        kind: Job
        metadata:
          name: {{ template "s3.name" . }}-coupled-or-updated-{% .plug.metadata.namespace %}
        spec:
          activeDeadlineSeconds: 360
          backoffLimit: 6
          ttlSecondsAfterFinished: 360
          template:
            metadata:
              annotations:
                sidecar.istio.io/inject: 'false'
            spec:
              automountServiceAccountToken: true
              restartPolicy: Never
              affinity:
                nodeAffinity:
                  requiredDuringSchedulingIgnoredDuringExecution:
                    nodeSelectorTerms:
                      - matchExpressions:
                          - key: kubernetes.io/arch
                            operator: In
                            values:
                              - amd64
              containers:
                - name: kubectl
                  image: registry.gitlab.com/bitspur/rock8s/images/kube-commands-aws
                  imagePullPolicy: Always
                  env:
                    - name: BUCKET_NAME
                      value: '{% .plugConfig.bucket %}'
                    - name: S3_ENDPOINT
                      value: '{% .socketConfig.endpoint %}'
                    - name: S3_ACCESS_KEY
                      valueFrom:
                        secretKeyRef:
                          name: {{ template "s3.name" . }}-secret
                          key: accessKey
                    - name: S3_SECRET_KEY
                      valueFrom:
                        secretKeyRef:
                          name: {{ template "s3.name" . }}-secret
                          key: secretKey
                  command:
                    - sh
                    - -c
                    - |
                      if [ "{% .plugConfig.create %}" != "0" ]; then
                        export S3CMD_CONFIG="/tmp/.s3cfg"
                        echo "[default]" > $S3CMD_CONFIG
                        echo "access_key = $S3_ACCESS_KEY" >> $S3CMD_CONFIG
                        echo "secret_key = $S3_SECRET_KEY" >> $S3CMD_CONFIG
                        echo "host_base = $S3_ENDPOINT" >> $S3CMD_CONFIG
                        echo "host_bucket = $S3_ENDPOINT" >> $S3CMD_CONFIG
                        echo "use_https = True" >> $S3CMD_CONFIG
                        chmod 600 $S3CMD_CONFIG
                        BUCKET_NAME="{% .plugConfig.bucket %}"
                        if s3cmd -c $S3CMD_CONFIG ls s3://$BUCKET_NAME > /dev/null 2>&1; then
                          echo "STATUS_MESSAGE: bucket $BUCKET_NAME already exists"
                        else
                          if s3cmd -c $S3CMD_CONFIG mb s3://$BUCKET_NAME; then
                            echo "STATUS_MESSAGE: created bucket $BUCKET_NAME"
                          else
                            echo "STATUS_MESSAGE: failed to create bucket $BUCKET_NAME"
                            exit 1
                          fi
                        fi
                        rm $S3CMD_CONFIG
                      fi
                      kubectl get pods -n {{ .Release.Namespace }} \
                        -l job-name={{ template "s3.name" . }}-coupled-or-updated-{% .plug.metadata.namespace %} \
                        --field-selector status.phase=Failed \
                        -o yaml | kubectl delete -f -
    - when: [decoupled]
      do: recreate
      stringTemplate: |
        apiVersion: batch/v1
        kind: Job
        metadata:
          name: {{ template "s3.name" . }}-decoupled-{% .plug.metadata.namespace %}
        spec:
          activeDeadlineSeconds: 360
          backoffLimit: 6
          ttlSecondsAfterFinished: 360
          template:
            metadata:
              annotations:
                sidecar.istio.io/inject: 'false'
            spec:
              automountServiceAccountToken: true
              restartPolicy: Never
              affinity:
                nodeAffinity:
                  requiredDuringSchedulingIgnoredDuringExecution:
                    nodeSelectorTerms:
                      - matchExpressions:
                          - key: kubernetes.io/arch
                            operator: In
                            values:
                              - amd64
              containers:
                - name: kubectl
                  image: registry.gitlab.com/bitspur/rock8s/images/kube-commands-aws
                  imagePullPolicy: Always
                  env:
                    - name: S3_ENDPOINT
                      value: '{{ .Values.endpoint }}'
                    - name: S3_ACCESS_KEY
                      valueFrom:
                        secretKeyRef:
                          name: {{ template "s3.name" . }}-secret
                          key: accessKey
                    - name: S3_SECRET_KEY
                      valueFrom:
                        secretKeyRef:
                          name: {{ template "s3.name" . }}-secret
                          key: secretKey
                  command:
                    - sh
                    - -c
                    - |
                      {%- if (and (not (eq .plugConfig.create "0")) (eq .plugConfig.preserve "0")) %}
                      export S3CMD_CONFIG="/tmp/.s3cfg"
                      echo "[default]" > $S3CMD_CONFIG
                      echo "access_key = $S3_ACCESS_KEY" >> $S3CMD_CONFIG
                      echo "secret_key = $S3_SECRET_KEY" >> $S3CMD_CONFIG
                      echo "host_base = $S3_ENDPOINT" >> $S3CMD_CONFIG
                      echo "host_bucket = $S3_ENDPOINT" >> $S3CMD_CONFIG
                      echo "use_https = True" >> $S3CMD_CONFIG
                      chmod 600 $S3CMD_CONFIG
                      BUCKET_NAME="{% .plugConfig.bucket %}"
                      if s3cmd -c $S3CMD_CONFIG rb s3://$BUCKET_NAME --force; then
                        echo "STATUS_MESSAGE: deleted bucket $BUCKET_NAME"
                      else
                        echo "STATUS_MESSAGE: failed to delete bucket $BUCKET_NAME"
                      fi
                      {%- end %}
                      kubectl get pods -n {{ .Release.Namespace }} \
                        -l job-name={{ template "s3.name" . }}-decoupled-{% .plug.metadata.namespace %} \
                        --field-selector status.phase=Failed \
                        -o yaml | kubectl delete -f -
{{- else }}
    - when: [coupled, updated]
      do: apply
      stringTemplate: |
        {%- if (eq .plugConfig.create "1") %}
        apiVersion: awsblueprints.io/v1alpha1
        kind: S3IRSA
        metadata:
          name: {% .plugConfig.bucket %}
          namespace: {{ .Release.Namespace }}
        spec:
          resourceConfig:
            name: {% .plugConfig.bucket %}
            providerConfigName: aws-provider-config
            region: {{ template "s3.aws-region" . }}
            deletionPolicy: {% (eq .plugConfig.preserve "1") | ternary "Orphan" "Delete" %}
        {%- end %}
    - when: [coupled, updated]
      do: apply
      template:
        apiVersion: awsblueprints.io/v1alpha1
        kind: DelegateS3IRSA
        metadata:
          name: '{% .plug.metadata.name %}-{% .plug.metadata.namespace %}'
        spec:
          serviceAccountName: '{% .plugConfig.serviceAccountName | default "default" %}'
          delegate:
            namespace: '{% .plug.metadata.namespace %}'
            s3irsa:
              name: '{% .plugConfig.bucket %}'
          resourceConfig:
            name: '{% .plugConfig.bucket %}'
            providerConfigName: aws-provider-config
            region: {{ template "s3.aws-region" . }}
{{- end }}

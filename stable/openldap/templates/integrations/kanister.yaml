{{- if .Values.persistence.kanister.enabled }}
apiVersion: integration.rock8s.com/v1beta1
kind: Plug
metadata:
  name: kanister-openldap
  labels:
    app.kubernetes.io/name: {{ template "openldap.name" . }}
    helm.sh/chart: {{ .Chart.Name }}-{{ .Chart.Version }}
    app.kubernetes.io/instance: {{ .Release.Name }}
    app.kubernetes.io/managed-by: {{ .Release.Service }}
spec:
  epoch: {{ now | unixEpoch | quote }}
  socket:
    name: kanister
    namespace: kanister
  config:
    blueprint: openldap
    schedule: {{ .Values.persistence.kanister.schedule | quote }}
    workload: {{ template "openldap.name" . }}
---
apiVersion: cr.kanister.io/v1alpha1
kind: Blueprint
metadata:
  name: openldap
actions:
  backup:
    kind: StatefulSet
    outputArtifacts:
      backup:
        keyValue:
          snapshot: "{{"{{"}} .Phases.backup.Output.snapshot {{"}}"}}"
    phases:
      - func: KubeTask
        name: backup
        objects:
          openldap:
            kind: Secret
            name: {{ template "openldap.name" . }}
            namespace: '{{ .Release.Namespace }}'
        args:
          image: registry.gitlab.com/bitspur/rock8s/images/kanukopia-openldap:2.4.57
          namespace: {{ .Release.Namespace }}
          command:
            - /bin/bash
            - -o
            - errexit
            - -o
            - pipefail
            - -c
            - |
              BACKUP_FILE="/tmp/ldapdump.ldif"
              export PASSWORD='{{"{{"}} .Options.password | default "" | toString {{"}}"}}'
              export PREFIX='{{"{{"}} .Options.prefix | default "{{ .Release.Namespace }}/{{ template "openldap.name" . }}" | toString {{"}}"}}'
              export KANUKOPIA_DEBUG='{{"{{"}} .Options.debug | default "" | toString {{"}}"}}'
              export PROFILE_JSON='{{"{{"}} toJson .Profile {{"}}"}}'
              rm "$BACKUP_FILE" 2>/dev/null || true

              HOST='{{ template "openldap.openldap-hostname" . }}'
              BIND_ID='cn=admin,{{ template "openldap.openldap-root-dn" . }}'
              BIND_PASSWORD='{{"{{"}} .Phases.backup.Secrets.openldap.Data.LDAP_ADMIN_PASSWORD | toString {{"}}"}}'
              ldapsearch -x -D "$BIND_ID" -w "$BIND_PASSWORD" -b '{{ template "openldap.openldap-root-dn" . }}' -H ldap://$HOST -LLL > $BACKUP_FILE

              SNAPSHOT="$(kanukopia restic backup --json "$BACKUP_FILE" | jq -r '.snapshots[0].id')"
              rm -f "$BACKUP_FILE"
              kando output snapshot "$SNAPSHOT"
          podOverride:
            containers:
              - name: container
                imagePullPolicy: Always
  restore:
    kind: StatefulSet
    phases:
      - func: KubeTask
        name: restore
        objects:
          openldap:
            kind: Secret
            name: {{ template "openldap.name" . }}
            namespace: '{{ .Release.Namespace }}'
        args:
          image: registry.gitlab.com/bitspur/rock8s/images/kanukopia-openldap:2.4.57
          namespace: {{ .Release.Namespace }}
          command:
            - bash
            - -o
            - errexit
            - -o
            - pipefail
            - -c
            - |
              BACKUP_FILE=ldapdump.ldif
              export PASSWORD='{{"{{"}} .Options.password | default "" | toString {{"}}"}}'
              export PREFIX='{{"{{"}} .Options.prefix | default "{{ .Release.Namespace }}/{{ template "openldap.name" . }}" | toString {{"}}"}}'
              export KANUKOPIA_DEBUG='{{"{{"}} .Options.debug | default "" | toString {{"}}"}}'
              export PROFILE_JSON='{{"{{"}} toJson .Profile {{"}}"}}'
              rm "$BACKUP_FILE" 2>/dev/null || true
              SNAPSHOT='{{"{{"}} .Options.snapshot | default "" | toString {{"}}"}}'
              if [ "$SNAPSHOT" = "" ]; then
                SNAPSHOT_TIME='{{"{{"}} .Options.snapshotTime | default "latest" | toString {{"}}"}}'
                SNAPSHOT="$(kanukopia find-restic-snapshot "$SNAPSHOT_TIME")"
              fi
              kanukopia restic restore "$SNAPSHOT" --target "$BACKUP_FILE"

              HOST='{{ template "openldap.openldap-hostname" . }}'
              BIND_ID='cn=admin,{{ template "openldap.openldap-root-dn" . }}'
              BIND_PASSWORD='{{"{{"}} .Phases.backup.Secrets.openldap.Data.LDAP_ADMIN_PASSWORD | toString {{"}}"}}'
              ldapadd -c -D "$BIND_ID" -w "$BIND_PASSWORD" -H ldap://$HOST -f $BACKUP_FILE || true

              rm -f "$BACKUP_FILE"
          podOverride:
            containers:
              - name: container
                imagePullPolicy: Always
  restorefrom:
    inputArtifactNames:
      - backup
    kind: StatefulSet
    phases:
      - func: KubeTask
        name: restorefrom
        objects:
          openldap:
            kind: Secret
            name: {{ template "openldap.name" . }}
            namespace: '{{ .Release.Namespace }}'
        args:
          image: registry.gitlab.com/bitspur/rock8s/images/kanukopia-openldap:2.4.57
          namespace: {{ .Release.Namespace }}
          command:
            - bash
            - -o
            - errexit
            - -o
            - pipefail
            - -c
            - |
              BACKUP_FILE=ldapdump.ldif
              export PASSWORD='{{"{{"}} .Options.password | default "" | toString {{"}}"}}'
              export PREFIX='{{"{{"}} .Options.prefix | default "{{ .Release.Namespace }}/{{ template "openldap.name" . }}" | toString {{"}}"}}'
              export KANUKOPIA_DEBUG='{{"{{"}} .Options.debug | default "" | toString {{"}}"}}'
              export PROFILE_JSON='{{"{{"}} toJson .Profile {{"}}"}}'
              rm "$BACKUP_FILE" 2>/dev/null || true
              SNAPSHOT='{{"{{"}} .ArtifactsIn.backup.KeyValue.snapshot | toString {{"}}"}}'
              kanukopia restic restore "$SNAPSHOT" --target "$BACKUP_FILE"

              HOST='{{ template "openldap.openldap-hostname" . }}'
              BIND_ID='cn=admin,{{ template "openldap.openldap-root-dn" . }}'
              BIND_PASSWORD='{{"{{"}} .Phases.backup.Secrets.openldap.Data.LDAP_ADMIN_PASSWORD | toString {{"}}"}}'
              ldapadd -c -D "$BIND_ID" -w "$BIND_PASSWORD" -H ldap://$HOST -f $BACKUP_FILE || true

              rm -f "$BACKUP_FILE"
          podOverride:
            containers:
              - name: container
                imagePullPolicy: Always
{{- end }}

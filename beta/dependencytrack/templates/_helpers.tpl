{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "dependency-track.name" }}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this
(by the DNS naming spec).
*/}}
{{- define "dependency-track.fullname" }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Calculate dependency-track certificate
*/}}
{{- define "dependency-track.dependency-track-certificate" }}
{{- if (not (empty .Values.ingress.dependencyTrack.certificate)) }}
{{- printf .Values.ingress.dependencyTrack.certificate }}
{{- else }}
{{- printf "%s-release-letsencrypt" .Release.Name }}
{{- end }}
{{- end }}

{{/*
Calculate dependency-track hostname
*/}}
{{- define "dependency-track.dependency-track-hostname" }}
{{- if (and .Values.config.frontend.hostname (not (empty .Values.config.frontend.hostname))) }}
{{- printf .Values.config.frontend.hostname }}
{{- else }}
{{- if .Values.ingress.dependencyTrack.enabled }}
{{- printf .Values.ingress.dependencyTrack.hostname }}
{{- else }}
{{- printf "%s-release.%s.svc.cluster.local" .Release.Name .Release.Namespace }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Calculate dependency-track base url
*/}}
{{- define "dependency-track.dependency-track-base-url" }}
{{- if (and .Values.config.frontend.baseUrl (not (empty .Values.config.frontend.baseUrl))) }}
{{- printf .Values.config.frontend.baseUrl }}
{{- else }}
{{- if .Values.ingress.dependencyTrack.enabled }}
{{- $hostname := ((empty (include "dependency-track.dependency-track-hostname" .)) | ternary .Values.ingress.dependencyTrack.hostname (include "dependency-track.dependency-track-hostname" .)) }}
{{- $protocol := (.Values.ingress.dependencyTrack.tls | ternary "https" "http") }}
{{- printf "%s://%s" $protocol $hostname }}
{{- else }}
{{- printf "http://%s" (include "dependency-track.dependency-track-hostname" .) }}
{{- end }}
{{- end }}
{{- end }}


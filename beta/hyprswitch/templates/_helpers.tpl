{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "hyperswitch.name" }}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this
(by the DNS naming spec).
*/}}
{{- define "hyperswitch.fullname" }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Calculate hyperswitch certificate
*/}}
{{- define "hyperswitch.hyperswitch-certificate" }}
{{- if (not (empty .Values.ingress.hyperswitch.certificate)) }}
{{- printf .Values.ingress.hyperswitch.certificate }}
{{- else }}
{{- printf "%s-release-letsencrypt" .Release.Name }}
{{- end }}
{{- end }}

{{/*
Calculate hyperswitch hostname
*/}}
{{- define "hyperswitch.hyperswitch-hostname" }}
{{- if (and .Values.config.frontend.hostname (not (empty .Values.config.frontend.hostname))) }}
{{- printf .Values.config.frontend.hostname }}
{{- else }}
{{- if .Values.ingress.hyperswitch.enabled }}
{{- printf .Values.ingress.hyperswitch.hostname }}
{{- else }}
{{- printf "%s-release.%s.svc.cluster.local" .Release.Name .Release.Namespace }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Calculate hyperswitch base url
*/}}
{{- define "hyperswitch.hyperswitch-base-url" }}
{{- if (and .Values.config.frontend.baseUrl (not (empty .Values.config.frontend.baseUrl))) }}
{{- printf .Values.config.frontend.baseUrl }}
{{- else }}
{{- if .Values.ingress.hyperswitch.enabled }}
{{- $hostname := ((empty (include "hyperswitch.hyperswitch-hostname" .)) | ternary .Values.ingress.hyperswitch.hostname (include "hyperswitch.hyperswitch-hostname" .)) }}
{{- $protocol := (.Values.ingress.hyperswitch.tls | ternary "https" "http") }}
{{- printf "%s://%s" $protocol $hostname }}
{{- else }}
{{- printf "http://%s" (include "hyperswitch.hyperswitch-hostname" .) }}
{{- end }}
{{- end }}
{{- end }}


{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "langfuse.name" }}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this
(by the DNS naming spec).
*/}}
{{- define "langfuse.fullname" }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Calculate langfuse certificate
*/}}
{{- define "langfuse.langfuse-certificate" }}
{{- if (not (empty .Values.ingress.langfuse.certificate)) }}
{{- printf .Values.ingress.langfuse.certificate }}
{{- else }}
{{- printf "%s-release-letsencrypt" .Release.Name }}
{{- end }}
{{- end }}

{{/*
Calculate langfuse hostname
*/}}
{{- define "langfuse.langfuse-hostname" }}
{{- if (and .Values.config.langfuse.hostname (not (empty .Values.config.langfuse.hostname))) }}
{{- printf .Values.config.langfuse.hostname }}
{{- else }}
{{- if .Values.ingress.langfuse.enabled }}
{{- printf .Values.ingress.langfuse.hostname }}
{{- else }}
{{- printf "%s-release.%s.svc.cluster.local" .Release.Name .Release.Namespace }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Calculate langfuse base url
*/}}
{{- define "langfuse.langfuse-base-url" }}
{{- if (and .Values.config.langfuse.baseUrl (not (empty .Values.config.langfuse.baseUrl))) }}
{{- printf .Values.config.langfuse.baseUrl }}
{{- else }}
{{- if .Values.ingress.langfuse.enabled }}
{{- $hostname := ((empty (include "langfuse.langfuse-hostname" .)) | ternary .Values.ingress.langfuse.hostname (include "langfuse.langfuse-hostname" .)) }}
{{- $protocol := (.Values.ingress.langfuse.tls | ternary "https" "http") }}
{{- printf "%s://%s" $protocol $hostname }}
{{- else }}
{{- printf "http://%s" (include "langfuse.langfuse-hostname" .) }}
{{- end }}
{{- end }}
{{- end }}

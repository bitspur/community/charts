{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "dittofeed.name" }}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this
(by the DNS naming spec).
*/}}
{{- define "dittofeed.fullname" }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Calculate dittofeed certificate
*/}}
{{- define "dittofeed.dittofeed-certificate" }}
{{- if (not (empty .Values.ingress.dittofeed.certificate)) }}
{{- printf .Values.ingress.dittofeed.certificate }}
{{- else }}
{{- printf "%s-release-letsencrypt" .Release.Name }}
{{- end }}
{{- end }}

{{/*
Calculate dittofeed hostname
*/}}
{{- define "dittofeed.dittofeed-hostname" }}
{{- if (and .Values.config.dittofeed.hostname (not (empty .Values.config.dittofeed.hostname))) }}
{{- printf .Values.config.dittofeed.hostname }}
{{- else }}
{{- if .Values.ingress.dittofeed.enabled }}
{{- printf .Values.ingress.dittofeed.hostname }}
{{- else }}
{{- printf "%s-release.%s.svc.cluster.local" .Release.Name .Release.Namespace }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Calculate dittofeed base url
*/}}
{{- define "dittofeed.dittofeed-base-url" }}
{{- if (and .Values.config.dittofeed.baseUrl (not (empty .Values.config.dittofeed.baseUrl))) }}
{{- printf .Values.config.dittofeed.baseUrl }}
{{- else }}
{{- if .Values.ingress.dittofeed.enabled }}
{{- $hostname := ((empty (include "dittofeed.dittofeed-hostname" .)) | ternary .Values.ingress.dittofeed.hostname (include "dittofeed.dittofeed-hostname" .)) }}
{{- $protocol := (.Values.ingress.dittofeed.tls | ternary "https" "http") }}
{{- printf "%s://%s" $protocol $hostname }}
{{- else }}
{{- printf "http://%s" (include "dittofeed.dittofeed-hostname" .) }}
{{- end }}
{{- end }}
{{- end }}

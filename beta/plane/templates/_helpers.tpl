{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "plane.name" }}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this
(by the DNS naming spec).
*/}}
{{- define "plane.fullname" }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Calculate plane certificate
*/}}
{{- define "plane.plane-certificate" }}
{{- if (not (empty .Values.ingress.plane.certificate)) }}
{{- printf .Values.ingress.plane.certificate }}
{{- else }}
{{- printf "%s-release-letsencrypt" .Release.Name }}
{{- end }}
{{- end }}

{{/*
Calculate plane hostname
*/}}
{{- define "plane.plane-hostname" }}
{{- if (and .Values.config.frontend.hostname (not (empty .Values.config.frontend.hostname))) }}
{{- printf .Values.config.frontend.hostname }}
{{- else }}
{{- if .Values.ingress.plane.enabled }}
{{- printf .Values.ingress.plane.hostname }}
{{- else }}
{{- printf "%s-release.%s.svc.cluster.local" .Release.Name .Release.Namespace }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Calculate plane base url
*/}}
{{- define "plane.plane-base-url" }}
{{- if (and .Values.config.frontend.baseUrl (not (empty .Values.config.frontend.baseUrl))) }}
{{- printf .Values.config.frontend.baseUrl }}
{{- else }}
{{- if .Values.ingress.plane.enabled }}
{{- $hostname := ((empty (include "plane.plane-hostname" .)) | ternary .Values.ingress.plane.hostname (include "plane.plane-hostname" .)) }}
{{- $protocol := (.Values.ingress.plane.tls | ternary "https" "http") }}
{{- printf "%s://%s" $protocol $hostname }}
{{- else }}
{{- printf "http://%s" (include "plane.plane-hostname" .) }}
{{- end }}
{{- end }}
{{- end }}
